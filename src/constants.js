// https://flatuicolors.com/palette/ca
export const genreColors = {
  comedy: '#ff9f43',
  drama: '#5f27cd',
  romance: '#f368e0',
  crime: '#576574',
  other: '#c8d6e5',
};
