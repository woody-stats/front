export const formatNumber = number =>
  (Math.round(number * 100) / 100).toLocaleDateString()
;
