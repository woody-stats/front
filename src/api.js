import axios from 'axios';

export const fetchSummaryStats = () => {
  return axios.get('/api/summary/')
    .then(response => response.data)
  ;
};

export const fetchDecadesStats = () => {
  return axios.get('/api/decades/')
    .then(response => response.data)
  ;
};

export const fetchTitles = () => {
  return axios.get('/api/titles/')
    .then(response => response.data)
  ;
};
